import movies.importer.Processor;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {

    public LowercaseProcessor(String sourceDir, String outputDir, boolean srcContainsHeader) {
        super(sourceDir, outputDir, srcContainsHeader);
    }

    @Override
    public ArrayList<String> process(ArrayList<String> input) {
        ArrayList<String> asLower = new ArrayList<String>();
        for (String s : input
        ) {
            s = s.toLowerCase();
            asLower.add(s);
        }
        return asLower;
    }
}
