import java.io.IOException;

public class ProcessingTest {
    public static void main(String[] args) throws IOException {
        LowercaseProcessor shrinker = new LowercaseProcessor("C:\\Kirill\\Dawson\\Java\\fall2020lab05\\foobar_folder", "C:\\Kirill\\Dawson\\Java\\fall2020lab05\\foobar_folder\\outputFromLP", true);
        shrinker.execute();
        RemoveDuplicates dRemover = new RemoveDuplicates("C:\\Kirill\\Dawson\\Java\\fall2020lab05\\foobar_folder\\outputFromLP", "C:\\Kirill\\Dawson\\Java\\fall2020lab05\\foobar_folder\\outputFromRD", false);
        dRemover.execute();
    }
}
