import movies.importer.Processor;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
    public RemoveDuplicates(String sourceDir, String outputDir, boolean srcContainsHeader) {
        super(sourceDir, outputDir, false);
    }

    @Override
    public ArrayList<String> process(ArrayList<String> input) {
        ArrayList<String> noDuplicatesArray = new ArrayList<String>();
        for (String s: input
        ) {
            if (!noDuplicatesArray.contains(s)) {
                noDuplicatesArray.add(s);
            }
        }
        return noDuplicatesArray;
    }
}
